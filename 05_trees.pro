domains
	tree_type = tree(integer, tree_type, tree_type); nil()

predicates
	tree_gen
	tree_print(tree_type)

clauses
	tree_print(nil) :-
		!.
	tree_print(tree(Root, Left, Right)) :-
		write(Root), nl,
		tree_print(Left),
		tree_print(Right).

goal
	tree_print(
		tree(	4,
			tree(	2,
				tree(1, nil, nil),
				tree(3, nil, nil)),
			tree(	5, nil, nil))).