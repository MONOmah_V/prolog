predicates
	max2(integer, integer, integer).
	max3(integer, integer, integer, integer).

	min2(integer, integer, integer).
	min3(integer, integer, integer, integer).

clauses
	max2(X, Y, X) :- X >= Y, !.
	max2(X, Y, Y).
	max3(X, Y, Z, X) :- X >= Y, X >= Z, !.
	max3(X, Y, Z, Y) :- Y >= Z, !.
	max3(X, Y, Z, Z).

	min2(X, Y, X) :- X <= Y, !.
	min2(X, Y, Y).
	min3(X, Y, Z, X) :- X <= Y, X <= Z, !.
	min3(X, Y, Z, Y) :- Y <= Z, !.
	min3(X, Y, Z, Z).

goal
	max2(2, 4, Max2);
	max3(4, 5, 5, Max3);
	min2(2, 4, Min2);
	min3(4, 5, 5, Min3).

