predicates
	power(integer Base, integer Exponent, integer Result)
	power(integer Base, integer Exponent, integer Accumulator, integer Result)
  
clauses
	power(Base, Exponent, Result) :- power(Base, Exponent, 1, Result).
	power(Base, Exponent, Accumulator, Result) :- 
		Exponent > 0, !,  
		NewExponent = Exponent - 1, 
		NewAccumulator = Base * Accumulator, 
		power(Base, NewExponent, NewAccumulator, Result).
	power(_, 0, Accumulator, Accumulator).

goal
	power(2, 10, Result).
