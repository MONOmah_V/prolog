domains
	id = integer
	fname = symbol
	lname = symbol
	telno = integer
	
predicates
	telrecord(id, telno)
	personrecord(id, fname, lname)
	
clauses
	personrecord(1, "Ivan", "Ivanov").
	personrecord(2, "Petr", "Petrov").
	personrecord(3, "Sidor", "Sidorov").
	personrecord(4, "Vasiliy", "Vasilev").
	telrecord(1, 1234567).
	telrecord(1, 3215476).
	telrecord(2, 2363598).
	telrecord(3, 2144356).
	telrecord(4, 5463712).

goal
	personrecord(Id, "Ivan", Lname) and telrecord(Id, Tel).
	%personrecord(Id, "Ivan", Lname) and telrecord(Id, Tel) or personrecord(Id, "Vasiliy", Lname) and telrecord(Id, Tel).