predicates
	factorial(integer N, integer Result)
	
clauses
	factorial(0, 1) :- !.
	factorial(N, Result) :- NN = N - 1, factorial(NN, ResultNN), Result = ResultNN * N.
	
goal
	factorial(6, Result).