domains
	car = symbol
	price = integer
        speed = integer
        type = symbol
        
predicates
	auto(Car, Price, Speed, Type)
	
clauses
	auto("BMV",100000, 230, sport).
	auto("Audi",200000, 300, supercar).
	auto("Bentley",300000, 400, muscle).
	auto("Porshe",104000, 240, sport).
	auto("Bugatti",130000, 260, supercar).
	auto("Chevrolet",206000, 310, muscle).
	auto("Dodge",123000, 240, sport).
	auto("Ferrari",199000, 300, sedan).
	
goal
	auto(Car,Price, Speed, Type), Price<150000.
