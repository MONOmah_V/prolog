domains
	intlist = integer*

predicates
	list_print(intlist List)
	list_length(intlist List, integer Length)
	list_ismember(integer Value, intlist List)
	list_append(intlist First, intlist Second, intlist Result)

clauses
	list_print([]) :-
		!, nl.
	list_print([H | T]) :-
		write(H, " "),
		list_print(T).
		
	list_length([], 0) :-
		!.
	list_length([_ | T], Length) :-
		list_length(T, NewLength),
		Length = NewLength + 1.

	list_ismember(Value, [Value | _]) :-
		!,
		write(Value, " is in list."),
		nl.
	list_ismember(Value, [_ | T]) :-
		list_ismember(Value, T).

	list_append([], List, List) :-
		!.
	list_append([H | T], List, [H | AppT]) :-
		list_append(T, List, AppT).

goal
	%List = [1, 2, 3, 4, 5],
	%list_print(List),
	%list_length(List, Length),
	%list_ismember(5, List).
	list_append([1, 2], [3, 4], List).