predicates
	male(string).
	female(string).
	parent(string Child, string Parent).
	grandparent(string Grandparent, string Child).

clauses
	male("Bob").
	male("Joe").
	male("John").
	male("Dylan").
	male("Henry").
	male("Jack").
	male("Matt").
	male("Peter").
	male("Richard").
	
	female("Alice").
	female("Amanda").
	female("Mallory").
	female("Andrea").
	female("Holly").	
	female("Jane").
	female("Lise").
	female("Rachel").	
	
	parent("Mallory", "Bob").
	parent("Mallory", "Alice").
	parent("John", "Bob").
	parent("John", "Alice").
	parent("Andrea", "Joe").
	parent("Andrea", "Amanda").
	parent("Dylan", "Joe").
	parent("Dylan", "Amanda").

	parent("Henry", "John").
	parent("Henry", "Andrea").
	parent("Jack", "John").
	parent("Jack", "Andrea").
	parent("Holly", "Dylan").
	parent("Holly", "Mallory").
	
	parent("Lisa", "Jack").
	parent("Jane", "Henry").
	parent("Rachel", "Holly").
	parent("Rachel", "Matt").
	
	parent("Richard", "Lisa").
	parent("Richard", "Peter").
	
	grandparent(Grandparent, Child) :-
		parent(Parent, Grandparent), parent(Child, Parent).
	
goal
	grandparent(GP, "Henry").
	%grandparent(GP, "Jack"),male(GP).
	%grandparent("Alice", C).

