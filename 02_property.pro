domains
	property =
		car(string Model, integer Price, integer Year);
		house(integer Area, string Address, integer Year)
	person = string

predicates
	ownership(person, property)

clauses
	ownership("Alice", house(100, "Moscow", 2010)).
	ownership("Alice", car("BMW", 100000, 2014)).
	ownership("Bob", car("Audi", 75000, 2012)).
	ownership("Mallory", house(200, "NY", 2000)).
	ownership("Mallory", house(150, "London", 1995)).

goal
	ownership("Alice", X).