predicates
	factorial(integer N, integer Result)
	factorial_helper(integer N, integer Result, integer Counter, integer Product)

clauses
	factorial(N, Result) :- factorial_helper(N, Result, 1, 1).
	
	factorial_helper(N, Result, Counter, Product) :-
		Counter <= N, !,
		NewProduct = Product * Counter,
		NewCounter = Counter + 1,
		factorial_helper(N, Result, NewCounter, NewProduct).
	factorial_helper(_, Result, _, Result).

goal
	factorial(6, Result).